# RS Config for Rogue Survivor: Still Alive #



Provides audio and graphical output configuration options to Rogue Survivor: Still Alive (https://gitlab.com/RogueSurvivor-StillAlive/StillAlive).

Steps to deploy:

1. Build with the matching version number to RogueSurvivor.exe

2. Place RSConfig.exe in the root Rogue Survivor: Still Alive game directory
