﻿using djack.RogueSurvivor;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Setup
{
    public class ConfigForm : Form
    {
        //private IContainer components; //@@MP - commented out as never initialized (Release 6)
        private Button b_SaveExit;
        private GroupBox gb_Video;
        private RadioButton rb_Video_GDI;
        private RadioButton rb_Video_MDX;
        private Button b_Exit;
        private GroupBox gb_Sound;
        private RadioButton rb_Sound_MDX;
        private Panel panel1;
        private Label l_GameVersion;
        private RadioButton rb_Sound_SFML;
        private RadioButton rb_Sound_NoSound;
        //@@MP (Release 5-5)
        private GroupBox gb_Window;
        private RadioButton rb_Window_Fullscreen;
        private RadioButton rb_Window_Windowed;
        private CheckBox cbxLogToFile; //@@MP (Release 6-2)

        protected override void Dispose(bool disposing)
        {
            /*if (disposing && this.components != null) //@@MP - commented out as components never initialized (Release 6)
                this.components.Dispose();*/
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.b_SaveExit = new Button();
            this.gb_Video = new GroupBox();
            this.rb_Video_GDI = new RadioButton();
            this.rb_Video_MDX = new RadioButton();
            this.b_Exit = new Button();
            this.gb_Sound = new GroupBox();
            this.rb_Sound_NoSound = new RadioButton();
            this.rb_Sound_SFML = new RadioButton();
            this.rb_Sound_MDX = new RadioButton();
            this.gb_Window = new GroupBox(); //@@MP (Release 5-5)
            this.rb_Window_Fullscreen = new RadioButton(); //@@MP (Release 5-5)
            this.rb_Window_Windowed = new RadioButton(); //@@MP (Release 5-5)
            this.panel1 = new Panel();
            this.l_GameVersion = new Label();
            this.cbxLogToFile = new CheckBox(); //@@MP (Release 6-2)
            this.gb_Video.SuspendLayout();
            this.gb_Sound.SuspendLayout();
            this.gb_Window.SuspendLayout(); //@@MP (Release 5-5)
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            this.b_SaveExit.Location = new Point(60, 111); //5
            this.b_SaveExit.Name = "b_SaveExit";
            this.b_SaveExit.Size = new Size(122, 23);
            this.b_SaveExit.TabIndex = 2;
            this.b_SaveExit.Text = "Save and Exit";
            this.b_SaveExit.UseVisualStyleBackColor = true;
            this.b_SaveExit.Click += new EventHandler(this.b_SaveExit_Click);
            this.gb_Video.AutoSize = true;
            this.gb_Video.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.gb_Video.Controls.Add((Control)this.rb_Video_GDI);
            this.gb_Video.Controls.Add((Control)this.rb_Video_MDX);
            this.gb_Video.Location = new Point(6, 3);
            this.gb_Video.Name = "gb_Video";
            this.gb_Video.Size = new Size(121, 80);
            this.gb_Video.TabIndex = 0;
            this.gb_Video.TabStop = false;
            this.gb_Video.Text = "Video";
            this.rb_Video_GDI.AutoSize = true;
            this.rb_Video_GDI.Location = new Point(7, 44);
            this.rb_Video_GDI.Name = "rb_Video_GDI";
            this.rb_Video_GDI.Size = new Size(50, 17);
            this.rb_Video_GDI.TabIndex = 1;
            this.rb_Video_GDI.TabStop = true;
            this.rb_Video_GDI.Text = "GDI+";
            this.rb_Video_GDI.UseVisualStyleBackColor = true;
            this.rb_Video_GDI.CheckedChanged += new EventHandler(this.rb_Video_GDI_CheckedChanged);
            this.rb_Video_MDX.AutoSize = true;
            this.rb_Video_MDX.Checked = true;
            this.rb_Video_MDX.Location = new Point(7, 20);
            this.rb_Video_MDX.Name = "rb_Video_MDX";
            this.rb_Video_MDX.Size = new Size(108, 17);
            this.rb_Video_MDX.TabIndex = 0;
            this.rb_Video_MDX.TabStop = true;
            this.rb_Video_MDX.Text = "Managed DirectX*";
            this.rb_Video_MDX.UseVisualStyleBackColor = true;
            this.rb_Video_MDX.CheckedChanged += new EventHandler(this.rb_Video_MDX_CheckedChanged);
            this.b_Exit.Location = new Point(190, 111); //135
            this.b_Exit.Name = "b_Exit";
            this.b_Exit.Size = new Size(121, 23);
            this.b_Exit.TabIndex = 3;
            this.b_Exit.Text = "Exit";
            this.b_Exit.UseVisualStyleBackColor = true;
            this.b_Exit.Click += new EventHandler(this.b_Exit_Click);
            this.gb_Sound.AutoSize = true;
            this.gb_Sound.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.gb_Sound.Controls.Add((Control)this.rb_Sound_NoSound);
            this.gb_Sound.Controls.Add((Control)this.rb_Sound_SFML);
            this.gb_Sound.Controls.Add((Control)this.rb_Sound_MDX);
            this.gb_Sound.Location = new Point(136, 3);
            this.gb_Sound.Name = "gb_Sound";
            this.gb_Sound.Size = new Size(121, 102);
            this.gb_Sound.TabIndex = 1;
            this.gb_Sound.TabStop = false;
            this.gb_Sound.Text = "Sound";
            this.rb_Sound_NoSound.AutoSize = true;
            this.rb_Sound_NoSound.Location = new Point(7, 66);
            this.rb_Sound_NoSound.Name = "rb_Sound_NoSound";
            this.rb_Sound_NoSound.Size = new Size(71, 17);
            this.rb_Sound_NoSound.TabIndex = 2;
            this.rb_Sound_NoSound.TabStop = true;
            this.rb_Sound_NoSound.Text = "No sound";
            this.rb_Sound_NoSound.UseVisualStyleBackColor = true;
            this.rb_Sound_NoSound.CheckedChanged += new EventHandler(this.rb_Sound_NoSound_CheckedChanged);
            this.rb_Sound_SFML.AutoSize = true;
            this.rb_Sound_SFML.Checked = true; //@@MP (Release 5-3)
            this.rb_Sound_SFML.Location = new Point(7, 44);
            this.rb_Sound_SFML.Name = "rb_Sound_SFML";
            this.rb_Sound_SFML.Size = new Size(71, 17);
            this.rb_Sound_SFML.TabIndex = 1;
            this.rb_Sound_SFML.TabStop = true;
            this.rb_Sound_SFML.Text = "SFML 2.4*";
            this.rb_Sound_SFML.UseVisualStyleBackColor = true;
            this.rb_Sound_SFML.CheckedChanged += new EventHandler(this.rb_Sound_SFML_CheckedChanged);
            this.rb_Sound_MDX.AutoSize = true;
            //this.rb_Sound_MDX.Checked = true; //@@MP (Release 5-3)
            this.rb_Sound_MDX.Location = new Point(7, 20);
            this.rb_Sound_MDX.Name = "rb_Sound_MDX";
            this.rb_Sound_MDX.Size = new Size(108, 17);
            this.rb_Sound_MDX.TabIndex = 0;
            this.rb_Sound_MDX.TabStop = true;
            this.rb_Sound_MDX.Text = "Managed DirectX";
            this.rb_Sound_MDX.UseVisualStyleBackColor = true;
            this.rb_Sound_MDX.CheckedChanged += new EventHandler(this.rb_Sound_MDX_CheckedChanged);
            //@@MP (Release 5-3) --->
            this.gb_Window.AutoSize = true;
            this.gb_Window.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.gb_Window.Controls.Add((Control)this.rb_Window_Windowed);
            this.gb_Window.Controls.Add((Control)this.rb_Window_Fullscreen);
            this.gb_Window.Location = new Point(262, 3);
            this.gb_Window.Name = "gb_Window";
            this.gb_Window.Size = new Size(121, 80);
            this.gb_Window.TabIndex = 3;
            this.gb_Window.TabStop = false;
            this.gb_Window.Text = "Window";
            this.rb_Window_Windowed.AutoSize = true;
            this.rb_Window_Windowed.Location = new Point(7, 44);
            this.rb_Window_Windowed.Name = "rb_Window_Windowed";
            this.rb_Window_Windowed.Size = new Size(90, 17);
            this.rb_Window_Windowed.TabIndex = 1;
            this.rb_Window_Windowed.TabStop = true;
            this.rb_Window_Windowed.Text = "Windowed";
            this.rb_Window_Windowed.UseVisualStyleBackColor = true;
            this.rb_Window_Windowed.CheckedChanged += new EventHandler(this.rb_Window_Windowed_CheckedChanged);
            this.rb_Window_Fullscreen.AutoSize = true;
            this.rb_Window_Fullscreen.Checked = true;
            this.rb_Window_Fullscreen.Location = new Point(7, 20);
            this.rb_Window_Fullscreen.Name = "rb_Window_Fullscreen";
            this.rb_Window_Fullscreen.Size = new Size(90, 17);
            this.rb_Window_Fullscreen.TabIndex = 0;
            this.rb_Window_Fullscreen.TabStop = true;
            this.rb_Window_Fullscreen.Text = "Fullscreen*";
            this.rb_Window_Fullscreen.UseVisualStyleBackColor = true;
            this.rb_Window_Fullscreen.CheckedChanged += new EventHandler(this.rb_Window_Fullscreen_CheckedChanged);
            // <---
            //@@MP (Release 6-2) --->
            this.cbxLogToFile.Text = "Write log file for troubleshooting";
            this.cbxLogToFile.CheckedChanged += new EventHandler(this.cbxLogToFile_CheckedChanged);
            this.cbxLogToFile.UseVisualStyleBackColor = true;
            this.cbxLogToFile.TabStop = true;
            this.cbxLogToFile.AutoSize = true;
            this.panel1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.cbxLogToFile.Dock = DockStyle.Right;
            // <---
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.panel1.Controls.Add((Control)this.gb_Sound);
            this.panel1.Controls.Add((Control)this.b_Exit);
            this.panel1.Controls.Add((Control)this.gb_Video);
            this.panel1.Controls.Add((Control)this.b_SaveExit);
            this.panel1.Controls.Add((Control)this.gb_Window); //@@MP (Release 5-3)
            this.panel1.Dock = DockStyle.Bottom;
            this.panel1.Location = new Point(0, 20); //,42
            this.panel1.Name = "panel1";
            this.panel1.Size = new Size(360, 137);
            this.panel1.TabIndex = 4;
            this.l_GameVersion.AutoSize = true;
            this.l_GameVersion.Dock = DockStyle.Top;
            this.l_GameVersion.Location = new Point(0, 0);
            this.l_GameVersion.Name = "l_GameVersion";
            this.l_GameVersion.Size = new Size(106, 13);
            this.l_GameVersion.TabIndex = 5;
            this.l_GameVersion.Text = "<game version here>";
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.ClientSize = new Size(360, 160);
            this.Controls.Add((Control)this.l_GameVersion);
            this.Controls.Add((Control)this.cbxLogToFile); //@@MP (Release 6-2)
            this.Controls.Add((Control)this.panel1);
            this.Name = nameof(ConfigForm);
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Rogue Survivor Config";
            this.Load += new EventHandler(this.SetupForm_Load);
            this.gb_Video.ResumeLayout(false);
            this.gb_Video.PerformLayout();
            this.gb_Sound.ResumeLayout(false);
            this.gb_Sound.PerformLayout();
            this.gb_Window.ResumeLayout(false);//@@MP (Release 5-3)
            this.gb_Window.PerformLayout();//@@MP (Release 5-3)
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        public ConfigForm()
        {
            this.InitializeComponent();
            this.l_GameVersion.Text = "* defaults"; //@@MP - change to the defaults label (Release 5-3)
        }

        private void b_SaveExit_Click(object sender, EventArgs e)
        {
            SetupConfig.Save();
            this.Close();
        }

        private void b_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SetupForm_Load(object sender, EventArgs e)
        {
            SetupConfig.Load();
            switch (SetupConfig.Video)
            {
                case SetupConfig.eVideo.VIDEO_MANAGED_DIRECTX:
                    this.rb_Video_MDX.Checked = true;
                    break;
                case SetupConfig.eVideo.VIDEO_GDI_PLUS:
                    this.rb_Video_GDI.Checked = true;
                    break;
            }
            switch (SetupConfig.Sound)
            {
                case SetupConfig.eSound.SOUND_MANAGED_DIRECTX:
                    this.rb_Sound_MDX.Checked = true;
                    break;
                case SetupConfig.eSound.SOUND_SFML:
                    this.rb_Sound_SFML.Checked = true;
                    break;
                case SetupConfig.eSound.SOUND_NOSOUND:
                    this.rb_Sound_NoSound.Checked = true;
                    break;
            }
            switch (SetupConfig.Window) //@@MP (Release 5-5)
            {
                case SetupConfig.eWindow.WINDOW_FULLSCREEN:
                    this.rb_Window_Fullscreen.Checked = true;
                    break;
                case SetupConfig.eWindow.WINDOW_WINDOWED:
                    this.rb_Window_Windowed.Checked = true;
                    break;
            }
            switch (SetupConfig.WriteLogToFile) //@@MP (Release 6-2)
            {
                case false:
                    this.cbxLogToFile.Checked = false; break;
                case true:
                    this.cbxLogToFile.Checked = true; break;
            }
        }

        private void rb_Video_MDX_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.rb_Video_MDX.Checked || SetupConfig.Video == SetupConfig.eVideo.VIDEO_MANAGED_DIRECTX)
                return;
            SetupConfig.Video = SetupConfig.eVideo.VIDEO_MANAGED_DIRECTX;
        }

        private void rb_Video_GDI_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.rb_Video_GDI.Checked || SetupConfig.Video == SetupConfig.eVideo.VIDEO_GDI_PLUS)
                return;
            SetupConfig.Video = SetupConfig.eVideo.VIDEO_GDI_PLUS;
        }

        private void rb_Sound_MDX_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.rb_Sound_MDX.Checked || SetupConfig.Sound == SetupConfig.eSound.SOUND_MANAGED_DIRECTX)
                return;
            SetupConfig.Sound = SetupConfig.eSound.SOUND_MANAGED_DIRECTX;
        }

        private void rb_Sound_SFML_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.rb_Sound_SFML.Checked || SetupConfig.Sound == SetupConfig.eSound.SOUND_SFML)
                return;
            SetupConfig.Sound = SetupConfig.eSound.SOUND_SFML;
        }

        private void rb_Sound_NoSound_CheckedChanged(object sender, EventArgs e)
        {
            if (!this.rb_Sound_NoSound.Checked || SetupConfig.Sound == SetupConfig.eSound.SOUND_NOSOUND)
                return;
            SetupConfig.Sound = SetupConfig.eSound.SOUND_NOSOUND;
        }

        private void rb_Window_Fullscreen_CheckedChanged(object sender, EventArgs e) //@@MP (Release 5-5)
        {
            if (!this.rb_Window_Fullscreen.Checked || SetupConfig.Window == SetupConfig.eWindow.WINDOW_FULLSCREEN)
                return;
            SetupConfig.Window = SetupConfig.eWindow.WINDOW_FULLSCREEN;
        }

        private void rb_Window_Windowed_CheckedChanged(object sender, EventArgs e) //@@MP (Release 5-5)
        {
            if (!this.rb_Window_Windowed.Checked || SetupConfig.Window == SetupConfig.eWindow.WINDOW_WINDOWED)
                return;
            SetupConfig.Window = SetupConfig.eWindow.WINDOW_WINDOWED;
        }

        private void cbxLogToFile_CheckedChanged(object sender, EventArgs e) //@@MP (Release 6-2)
        {
            if (this.cbxLogToFile.Checked)
                SetupConfig.WriteLogToFile = true;
            else
                SetupConfig.WriteLogToFile = false;
        }
    }
}
