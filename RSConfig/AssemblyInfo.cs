﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("-")]
[assembly: AssemblyProduct("Rogue Survivor: Still Alive config")]
[assembly: AssemblyCopyright("Jacques Ruiz (roguedjack) 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyDescription("Sets audio and video engines for Rogue Survivor")]
[assembly: ComVisible(false)]
[assembly: Guid("7313e252-ec39-4b0e-845a-91f886bd3fe9")]
[assembly: NeutralResourcesLanguage("en")]
[assembly: AssemblyTitle("Config")]
[assembly: AssemblyFileVersion("2.11.0.2")]
[assembly: AssemblyVersion("2.11.0.2")]
